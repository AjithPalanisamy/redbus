const express=require("express");
const router=express.Router();
const user=require('../model/userdb');
const bcrypt=require("bcryptjs");
const jwt=require("jsonwebtoken");
const auth=require("../auth/auth");

router.post('/register',async(req,res)=>
{
    try{
        const {name,age,gender,contact,password,confirmPassword}=req.body;
        const result=await user.find({email:`${req.body.email}`});
        if (!(name && age && gender && contact && password && confirmPassword )) 
        {
            res.send("All inputs are required");
        }
        else if(password != confirmPassword)
        {
            res.send("Password and Confirm Password must be same.");
        }
        else if(result.length==0)
        {
            let userObj=new user({
                name:name,
                age:age,
                gender:gender,
                contact:contact,
                email:email,
                password:password
            });
            let encrypted=await bcrypt.hash(password,10);
            userObject.password=encrypted;
            await userObject.save();
            res.send("User successfully registered !!");
        }
        else
        {
            res.send("User with this Email is already Exist..Please Login");
        }
    }
    catch(err)
    {
        res.send(err);
    }

});

router.post('/login',async(req,res)=>
{
    try{
        let {email,password}=req.body;
        const userObject=await user.findOne({email:`${email}`});
        if(userObject!=null && await bcrypt.compare(password, userObject.password) )
        {
            let id=userObject._id;
            const token = jwt.sign(
                {email,id},
                process.env.TOC,
                {
                  expiresIn: "2h",
                }
              );
              console.log(token);
            res.send(`Welcome ${userObject.name}\n Token : ${token}`);
        }
        else if(userObject!=null)
        {
           res.send("Incorrect Password");
        }
        else
        {
            res.send("User with this email does not exist.Please Register!!");
        }
    }
    catch(err)
    {
        res.send(err);
    }
});

router.post('/profile',auth,async(req,res)=>
{
    const userObject=await user.findOne({email:`${req.user}`});
    res.send(userObject);
});






module.exports=router;